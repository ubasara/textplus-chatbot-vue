import {
  App,
  createApp,
  runAuth,
  createOidcSettings,
  CHAT_APP_API_URL,
  CHAT_APP_HOME_PATH,
  CHAT_APP_OIDC_REDIRECT_PATH,
  modelStore,
  conversationsStore,
  createStore,
  createRouterInstance,
} from "@/export";


//console.log(import.meta.env.VITE_APP_OIDC_BASE_URL, import.meta.env.VITE_APP_OIDC_REALM,  import.meta.env.VITE_APP_OIDC_CLIENT_ID, import.meta.env.VITE_APP_HOME_PATH)

// Merge the customOidcStore into the oidcStoreModule
const store = createStore({
  modules: {
    conversationsStore,
    modelStore,
  },
});

const oidcSettings = createOidcSettings({
  authority: `${import.meta.env.VITE_APP_OIDC_BASE_URL}/auth/realms/${import.meta.env.VITE_APP_OIDC_REALM}`,
  clientId: import.meta.env.VITE_APP_OIDC_CLIENT_ID,
  redirectUri: `${import.meta.env.VITE_APP_URL}${import.meta.env.VITE_APP_OIDC_REDIRECT_PATH}`
});

runAuth(oidcSettings);

// Create router with base path
const router = createRouterInstance(import.meta.env.VITE_APP_HOME_PATH, import.meta.env.VITE_APP_REDIRECT_PATH);

createApp(App)
  .provide(CHAT_APP_API_URL, import.meta.env.VITE_APP_API_URL)
  .provide(CHAT_APP_HOME_PATH, import.meta.env.VITE_APP_HOME_PATH)
  .use(store)
  .use(router)
  .mount('#app');

