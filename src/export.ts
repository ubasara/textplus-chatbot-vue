import "./assets/main.css";
import App from "./App.vue";
import { createApp as createVueApp } from "vue";
import { createVuetify } from "vuetify";

import * as components from "vuetify/components";
import * as directives from "vuetify/directives";
import { aliases, mdi } from "vuetify/iconsets/mdi";
import "@mdi/font/css/materialdesignicons.css";
import { CHAT_APP_API_URL, CHAT_APP_OIDC_REDIRECT_PATH, CHAT_APP_HOME_PATH } from "@/utils/keys";
import { createStore } from 'vuex';
import conversationsStore from '@/stores/conversationsStore';
import modelStore from '@/stores/modelStore'; 
import { runAuth, createOidcSettings } from "@/stores/oidc";
import { createRouterInstance } from "./router";

const icons = {
  defaultSet: "mdi",
  aliases,
  sets: {
    mdi,
  },
};
const vuetify = createVuetify({
  components,
  icons,
  directives,
  theme: {
    themes: {
      light: {
        colors: {
          primary: "#CD0022",
          secondary: "#245787",
        },
      },
    },
  },
});

function createApp(component: any) {
  // create an app instance
  const app = createVueApp(component);
  // and add the plugins
  app.use(vuetify);
  // return app for mounting
  return app;
}

export { App, createApp, createRouterInstance, runAuth, createOidcSettings, CHAT_APP_API_URL, CHAT_APP_HOME_PATH, CHAT_APP_OIDC_REDIRECT_PATH, modelStore, conversationsStore, createStore };
