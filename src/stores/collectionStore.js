import { createStore } from 'vuex';

export default {
  namespaced: true,
  state() {
    return {
      llmName: "", // Initialize state with an empty array
      generationModel: "",
      embeddingsModel: ""
      prompt: "You are a helpful assistant."
      temperature: 0.5
    };
  },
  mutations: {
    setLlmName(state, name) {
        state.llmName = name;
    },
    setGenerationModel(state, genModel) {
        state.generationModel = genModel
    },
    setEmbeddingsModel(state, embModel) {
        state.embeddingsModel = embModel
    },
    setPrompt(state, prompt) {
        state.prompt = prompt
    },
    setTemp(state, temperature) {
        state.temperature = temperature
    }
  },
  actions: {

  },
  getters: {

  }