import { useOidcStore } from "vue3-oidc";
import { getAuthHeaders } from '@/utils/auth';

export default {
  namespaced: true,
  state() {
    return {
      modelList: [],
      selectedModel: {},
    };
  },
  mutations: {
    setModelList(state, models) {
      state.modelList = models;
    },
    setSelectedModel(state, model = undefined) {
      state.selectedModel = model;
    },
  },
  actions: {
    async fetchAllModels({ commit }, url) {
      const newModelList = [];
      let baseSettings = {};

      try {
        const headers = await getAuthHeaders();
        if (!headers.Authorization) {
          throw new Error('No valid authorization token available');
        }

        // Fetch custom models
        const customModelsResponse = await fetch(`${url}/get-collections/`, { headers });
        
        if (!customModelsResponse.ok) {
          if (customModelsResponse.status === 401) {
            console.error("[API] Unauthorized error while fetching custom models");
            throw new Error('Unauthorized - Please log in again');
          }
          throw new Error(`HTTP error! Status: ${customModelsResponse.status}`);
        }

        const customModelsData = await customModelsResponse.json();
        
        // Process custom models
        customModelsData.models.forEach((model) => {
          const modifiedModel = {
            type: "custom",
            name: model.collection_name,
            id: model.uuid,
            llm: model.llmName,
            temperature: model.temperature,
            prompt: model.prompt,
            cmetadata: model.cmetadata,
          };
          newModelList.push(modifiedModel);
        });

        baseSettings = customModelsData.base;

        // Fetch base models
        const baseModelsResponse = await fetch(`${url}/api/v1/models`, { headers });
        
        if (!baseModelsResponse.ok) {
          if (baseModelsResponse.status === 401) {
            console.error("[API] Unauthorized error while fetching base models");
            throw new Error('Unauthorized - Please log in again');
          }
          throw new Error(`HTTP error! Status: ${baseModelsResponse.status}`);
        }

        const baseModelsData = await baseModelsResponse.json();
        
        baseModelsData.data.forEach((model) => {
          newModelList.push({
            type: "base",
            name: model.name,
            llm: model.id,
            id: model.id,
            temperature: baseSettings.base_temp,
            prompt: baseSettings.base_prompt,
          });
        });

        commit("setModelList", newModelList);
      } catch (error) {
        console.error("[API] Error fetching models:", error);
      }
    },

    async deleteModelAction({ dispatch }, { model, url }) {
      try {
        const headers = await getAuthHeaders();
        if (!headers.Authorization) {
          throw new Error('No valid authorization token available');
        }

        const response = await fetch(`${url}/delete-collection?collection_name=${model.name}`, {
          method: "DELETE",
          headers
        });

        if (!response.ok) {
          if (response.status === 401) {
            console.error("[API] Unauthorized error while deleting model");
            throw new Error('Unauthorized - Please log in again');
          }
          throw new Error(`Failed to delete model: ${response.statusText}`);
        }

        const data = await response.json();
        console.log("[API] Model deleted successfully");

        // After successful deletion, fetch the updated model list with the correct action name
        await dispatch('fetchAllModels', url);

      } catch (error) {
        console.error("[API] Error deleting model:", error);
        throw error;
      }
    },
  },
  getters: {
    getModelList(state) {
      return state.modelList;
    },
    selectedModel(state) {
      return state.selectedModel;
    },
    getModelsByType: (state) => (type) => {
      return state.modelList.filter((model) => model.type === type);
    },
    getModelById: (state) => (id) => {
      return state.modelList.find((model) => model.id === id);
    },
  }
};
