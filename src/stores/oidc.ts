import { WebStorageStateStore } from "oidc-client-ts";
import { unref } from "vue";
import type { VueOidcSettings } from "vue3-oidc";
import { createOidc, useAuth, useOidcStore } from "vue3-oidc";
import type { User } from "oidc-client-ts";

const { state } = useOidcStore();
const { setRedirectUri } = useAuth();

// First, create an interface for the settings parameters
interface OidcSettingsParams {
  authority: string;
  clientId: string;
  redirectUri: string;
}

// Modify the function to accept an object parameter
export function createOidcSettings({ authority, clientId, redirectUri }: OidcSettingsParams): VueOidcSettings {
  return {
    authority: authority,
    client_id: clientId,
    redirect_uri: redirectUri,
    scope: "openid profile",
    response_type: "code",
    loadUserInfo: true,
    userStore: new WebStorageStateStore({
      prefix: "vue3-oidc",
      store: window.sessionStorage,
    }),
    automaticSilentRenew: true,
    monitorSession: true,
    silent_redirect_uri: location.origin + "/silent-renew.html",
    stateStore: new WebStorageStateStore({ store: window.sessionStorage }), // Add state store
    onSigninRedirectCallback(user) {
      const state = sessionStorage.getItem("oidc.state");
      if (!state) {
        console.warn("No state found in storage");
        location.href = unref(state).redirect_uri || "/";
        return;
      }
      location.href = unref(state).redirect_uri || "";
    },
    onBeforeSigninRedirectCallback() {
      const currentPath = location.pathname + location.search;
      sessionStorage.setItem("oidc.state", currentPath);
      setRedirectUri(currentPath);
    },
  };
}

export function runAuth(oidcSettings: VueOidcSettings) {
  
  createOidc({
    oidcSettings: oidcSettings,
    auth: false,
    refreshToken: {
      enable: true,
      time: 300 * 1000, // Refresh 5 minutes before expiration instead of 10 seconds
    },
    events: {
      addUserLoaded: (user: User) => {
        const expiresAt = user.expires_at ? new Date(user.expires_at * 1000).toISOString() : 'unknown';
        const now = new Date().toISOString();
        console.log("[OIDC] User loaded at:", now);
        console.log("[OIDC] Token expires at:", expiresAt);
        
        // Calculate and log the time until expiration
        if (user.expires_at) {
          const timeUntilExpiry = user.expires_at * 1000 - Date.now();
          console.log(`[OIDC] Time until token expiration: ${Math.round(timeUntilExpiry / 1000)}s`);
        }
      },
      addUserSignedOut: () => {
        console.log("[OIDC] User signed out at:", new Date().toISOString());
      },
      addAccessTokenExpiring: () => {
        const now = Date.now();
        const expiresAt = state.value.user?.expires_at ? state.value.user.expires_at * 1000 : 0;
        const remainingTime = Math.round((expiresAt - now) / 1000);
        
        console.log(`[OIDC] Access token expiring - Current time: ${new Date(now).toISOString()}`);
        console.log(`[OIDC] Access token expiring - Expires at: ${new Date(expiresAt).toISOString()}`);
        console.log(`[OIDC] Access token expiring - Remaining time: ${remainingTime}s`);
        
        if (remainingTime <= 0) {
          console.warn("[OIDC] Token has already expired, forcing immediate refresh");
        }

        // Force reload user info to ensure we have latest state
        state.value.userManager?.getUser().then((user: User | null) => {
          if (user?.expires_at) {
            const freshExpiresAt = new Date(user.expires_at * 1000).toISOString();
            console.log("[OIDC] Current token status after reload:", {
              current_time: new Date().toISOString(),
              expires_at: freshExpiresAt,
              remaining_seconds: Math.round((user.expires_at * 1000 - Date.now()) / 1000),
              scopes: user.scopes,
            });
          }
        });
      },
      addAccessTokenExpired: () => {
        console.log("[OIDC] Access token has expired at:", new Date().toISOString());
        console.log("[OIDC] Attempting silent refresh...");
        
        // Attempt silent refresh
        state.value.userManager?.signinSilent()
          .then((user) => {
            if (user?.expires_at) {
              console.log("[OIDC] Silent refresh successful. New token expires at:", 
                new Date(user.expires_at * 1000).toISOString());
            }
          })
          .catch(error => {
            console.error("[OIDC] Silent refresh failed at:", new Date().toISOString(), error);
          });
      },
      addSilentRenewError: (error) => {
        console.error("[OIDC] Silent renew error at:", new Date().toISOString());
        console.error("[OIDC] Error details:", error);
        
        // Check if error is due to iframe issues
        if (error?.message?.includes('requestStorageAccess')) {
          console.warn("[OIDC] Storage access error detected - this may affect token refresh");
          // Attempt to force a new sign-in if storage access is the issue
          state.value.userManager?.signinRedirect().catch(e => 
            console.error("[OIDC] Failed to redirect for new sign-in:", e)
          );
        }
      }
    },
    redirectUriKey: oidcSettings.redirect_uri,
  });
}
