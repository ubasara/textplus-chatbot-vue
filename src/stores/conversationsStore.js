import { useOidcStore } from "vue3-oidc";
import { getAuthHeaders } from '@/utils/auth';

export default {
  namespaced: true,
  state() {
    return {
      conversations: [], // Initialize state with an empty array
      selectedConversation: null, // Added state for selectedConversation
    };
  },
  mutations: {
    // Mutation to set conversations in the state
    setConversations(state, conversations) {
      // Ensure isGenerating is set to false for all messages in all conversations
      const conversationsWithGeneratingState = conversations.map(conv => ({
        ...conv,
        messages: conv.messages.map(msg => ({
          ...msg,
          isGenerating: false
        }))
      }));
      state.conversations = conversationsWithGeneratingState;
    },
    // Mutation to set the selected conversation
    setSelectedConversation(state, conversation) {
      if (conversation) {
        // Ensure isGenerating is set to false for all messages
        const conversationWithGeneratingState = {
          ...conversation,
          messages: conversation.messages.map(msg => ({
            ...msg,
            isGenerating: false
          }))
        };
        state.selectedConversation = conversationWithGeneratingState;
      } else {
        state.selectedConversation = null;
      }
    },
    // Mutation to trigger a conversation update
    triggerConversationUpdate(state) {
      // This mutation doesn't need to modify state
      // It will be used as a trigger for the watcher
      state.conversations = [...state.conversations];
    },
    // Mutation to update a conversation with a new message
    setNewMessage(state, { conversationId, message }) {
      // Update the conversation in the list of conversations
      const conversation = state.conversations.find(c => c.id === conversationId);
      if (conversation) {
        // Add isGenerating property if not present
        if (!message.hasOwnProperty('isGenerating')) {
          message.isGenerating = false;
        }
        conversation.messages.push(message);
      }

      // Ensure selectedConversation is up-to-date if it's the same conversation
      if (state.selectedConversation && state.selectedConversation.id === conversationId) {
        state.selectedConversation = conversation; // Reassign to ensure reactivity
      }
    },
    updateLastMessageContent(state, { conversationId, content }) {
      const conversation = state.conversations.find(c => c.id === conversationId);
      if (conversation && conversation.messages.length > 0) {
        const lastMessage = conversation.messages[conversation.messages.length - 1];
        lastMessage.content = content;
      }

      // Ensure selectedConversation is up-to-date if it's the same conversation
      if (state.selectedConversation && state.selectedConversation.id === conversationId) {
        state.selectedConversation = conversation; // Reassign to ensure reactivity
      }
    },
    // Mutation to update the metadata of the last message in a conversation
    updateLastMessageMetadata(state, { conversationId, metadata }) {
      const conversation = state.conversations.find(c => c.id === conversationId);
      if (conversation && conversation.messages.length > 0) {
        const lastMessage = conversation.messages[conversation.messages.length - 1];
        lastMessage.metadata = { ...lastMessage.metadata, ...metadata };
      }

      // Ensure selectedConversation is up-to-date if it's the same conversation
      if (state.selectedConversation && state.selectedConversation.id === conversationId) {
        state.selectedConversation = conversation; // Reassign to ensure reactivity
      }
    },
    removeLastAssistantAndUser(state) {
      state.conversations.forEach(conversation => {
        // Check if there are messages in the conversation
        if (conversation.messages.length > 0) {
          // Find the index of the last 'assistant' message
          const lastAssistantIndex = conversation.messages.map(m => m.role).lastIndexOf('assistant');
          if (lastAssistantIndex !== -1) {
            conversation.messages.splice(lastAssistantIndex, 1);
          }
  
          // Find the index of the last 'user' message
          const lastUserIndex = conversation.messages.map(m => m.role).lastIndexOf('user');
          if (lastUserIndex !== -1) {
            conversation.messages.splice(lastUserIndex, 1);
          }
        }
  
        // Ensure selectedConversation is up-to-date if it's the same conversation
        if (state.selectedConversation && state.selectedConversation.id === conversation.id) {
          state.selectedConversation = conversation; // Reassign to ensure reactivity
        }
      });
    },
    // Add new mutation to update isGenerating state
    updateMessageGeneratingState(state, { conversationId, messageIndex, isGenerating }) {
      const conversation = state.conversations.find(c => c.id === conversationId);
      if (conversation && conversation.messages[messageIndex]) {
        conversation.messages[messageIndex].isGenerating = isGenerating;
      }

      // Ensure selectedConversation is up-to-date if it's the same conversation
      if (state.selectedConversation && state.selectedConversation.id === conversationId) {
        state.selectedConversation = conversation; // Reassign to ensure reactivity
      }
    },
  },
  actions: {
    // Action to fetch conversations from the API
    async fetchConversations({ commit }, url) {
      try {
        const headers = await getAuthHeaders();
        if (!headers.Authorization) {
          throw new Error('No valid authorization token available');
        }

        const response = await fetch(`${url}/users/conversations`, {
          headers
        });

        if (!response.ok) {
          if (response.status === 401) {
            console.error("[API] Unauthorized error while fetching conversations");
            throw new Error('Unauthorized - Please log in again');
          }
          throw new Error(`Failed to fetch conversations: ${response.statusText}`);
        }

        const text = await response.text();
        try {
          const data = JSON.parse(text);
          commit("setConversations", data.conversations);

          if (data.conversations.length > 0) {
            const latestConversation = data.conversations.reduce((prev, current) => {
              const prevTime = prev.last_message || prev.created_at;
              const currentTime = current.last_message || current.created_at;
              return prevTime > currentTime ? prev : current;
            });
            commit("setSelectedConversation", latestConversation);
          }
        } catch (parseError) {
          console.error("[API] Failed to parse conversations JSON:", parseError);
        }
      } catch (error) {
        console.error('[API] Error fetching conversations:', error);
      }
    },

    async renameConversation({ dispatch }, { conversationId, userId, newTopic, url }) {
      try {
        const headers = await getAuthHeaders();
        if (!headers.Authorization) {
          throw new Error('No valid authorization token available');
        }

        const response = await fetch(`${url}/conversations/rename`, {
          method: 'PUT',
          headers,
          body: JSON.stringify({
            user_id: userId,
            conversation_id: conversationId,
            new_topic: newTopic
          }),
        });

        if (!response.ok) {
          const errorData = await response.json();
          throw new Error(`Failed to rename conversation: ${JSON.stringify(errorData)}`);
        }

        // Refresh conversations after renaming
        await dispatch('fetchConversations', url);

      } catch (error) {
        console.error('Error renaming conversation:', error);
        throw error;
      }
    },

    async deleteConversation({ commit, dispatch, state }, { conversationId, userId, url }) {
      try {
        const headers = await getAuthHeaders();
        if (!headers.Authorization) {
          throw new Error('No valid authorization token available');
        }

        const response = await fetch(`${url}/conversations/${conversationId}`, {
          method: "DELETE",
          headers,
          body: JSON.stringify({ user_id: userId, conversation_id: conversationId }),
        });

        if (!response.ok) {
          if (response.status === 401) {
            console.error("[API] Unauthorized error while deleting conversation");
            throw new Error('Unauthorized - Please log in again');
          }
          const errorData = await response.json();
          throw new Error(`Failed to delete conversation: ${errorData}`);
        }

        // Clear selected conversation if it was the one that was deleted
        if (state.selectedConversation && state.selectedConversation.id === conversationId) {
          commit('setSelectedConversation', null);
        }

        await dispatch("fetchConversations", url);
      } catch (error) {
        console.error('[API] Error deleting conversation:', error);
      }
    },
    setSelectedConversation({ commit }, conversation) {
      commit('setSelectedConversation', conversation);
    },
    // Action to send a message and update the conversation in the state
    async updateConversationAction({ commit, state }, { conversationId, messages, url }) {
      try {
        const headers = await getAuthHeaders();
        if (!headers.Authorization) {
          throw new Error('No valid authorization token available');
        }

        const response = await fetch(`${url}/update_conversation`, {
          method: "POST",
          headers,
          body: JSON.stringify({
            id: conversationId,
            messages: messages
          }),
        });

        if (!response.ok) {
          if (response.status === 401) {
            console.error("[API] Unauthorized error while updating conversation");
            throw new Error('Unauthorized - Please log in again');
          }
          if (response.status === 403) {
            console.error("[API] User is not part of this conversation");
            throw new Error('Access denied: You are not a participant in this conversation');
          }
          const errorData = await response.json();
          throw new Error(`Failed to update conversation: ${errorData.detail}`);
        }

        const data = await response.json();
        // Update the conversation in the store with the returned data
        commit('setConversations', state.conversations.map(conv => 
          conv.id === conversationId 
            ? { ...conv, messages: data.conversation.messages, last_message_at: data.conversation.last_message_at }
            : conv
        ));
      } catch (error) {
        console.error("[API] Error updating conversation:", error);
        throw error;
      }
    }
    
  },
  getters: {
    conversations(state) {
      return state.conversations;
    },
    selectedConversation(state) {
      return state.selectedConversation;
    },
  },
};
