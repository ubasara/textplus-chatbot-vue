import {
  createRouter,
  createWebHistory,
  RouterView,
  type RouteRecordRaw,
} from "vue-router";
import ChatbotOidcCallback from "@/views/Chatbot.vue";
import Home from "@/views/Home.vue";

export function createRouterInstance(homePath = '/chatbot', redirectPath= '/' ) {
  const routes: RouteRecordRaw[] = [
    {
      path: homePath,
      name: "Home",
      component: Home,
    },
    {
      path: redirectPath,
      name: "OidcCallback",
      component: ChatbotOidcCallback,
    },
  ];

  return createRouter({
    history: createWebHistory(),
    routes: routes,
  });
}
