// utils/keys.js
export const CHAT_APP_API_URL = Symbol("CHAT_APP_API_URL");
export const CHAT_APP_HOME_PATH = Symbol("CHAT_APP_HOME_PATH");
export const CHAT_APP_OIDC_BASE_URL = Symbol("CHAT_APP_OIDC_BASE_URL");
export const CHAT_APP_OIDC_CLIENT_ID = Symbol("CHAT_APP_OIDC_CLIENT_ID");
export const CHAT_APP_OIDC_REALM = Symbol("CHAT_APP_OIDC_REALM");
export const CHAT_APP_OIDC_REDIRECT_PATH = Symbol('CHAT_APP_OIDC_REDIRECT_PATH');
