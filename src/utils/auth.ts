import { useOidcStore } from "vue3-oidc";

interface AuthHeaders {
  Authorization: string;
  "Content-Type": string;
}

export async function getLatestAccessToken(): Promise<string | null> {
  const { state } = useOidcStore();
  
  try {
    // First try to get fresh user info
    const user = await state.value.userManager?.getUser();
    if (user?.access_token) {
      return user.access_token;
    }
    
    // If that fails, try silent refresh
    const refreshedUser = await state.value.userManager?.signinSilent();
    if (refreshedUser?.access_token) {
      return refreshedUser.access_token;
    }
    
    console.error("[Auth] Failed to get valid access token");
    return null;
  } catch (error) {
    console.error("[Auth] Error getting access token:", error);
    return null;
  }
}

// Helper function to add authorization header with latest token
export async function getAuthHeaders(): Promise<AuthHeaders> {
  const token = await getLatestAccessToken();
  return {
    "Authorization": `Bearer ${token}`,
    "Content-Type": "application/json"
  };
} 