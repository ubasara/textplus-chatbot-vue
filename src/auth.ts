import { UserManager, WebStorageStateStore, User } from "oidc-client-ts";
import jwt_decode from "jwt-decode";
import store from "@/stores/store";

let userManager: UserManager;

const currentURL = document.location.origin;
fetch(currentURL + "/config.json", { 'method': "GET" })
  .then(config => {
    config.json()
      .then(configuration => {
        configuration['SSO_SETTINGS']['userStore'] = new WebStorageStateStore({ store: window.localStorage });
        userManager = new UserManager(configuration['SSO_SETTINGS']);

        userManager.events.addUserLoaded(newUser => {
          //console.log("USER LOADED EVENT");
          userManager.storeUser(newUser);
          userManager.getUser().then(usr => {
            store.dispatch("getUserInfo", usr?.profile.sub) // load the user from my api
          });
        });

        userManager.events.addAccessTokenExpired(() => {
          userManager.signoutRedirect();
        })

        userManager.events.addSilentRenewError(error => {
          //console.log("ERROR RENEWING ACCESS TOKEN.");
          //console.log(error);
        })
      })
  })

export default class AuthService {

public static getUser(): Promise<User | null> {
return userManager.getUser();
}

public static async login(): Promise<void> {
return userManager.signinRedirect();
}

public static logout(): Promise<void> {
localStorage.clear();
sessionStorage.clear();
return userManager.signoutRedirect();
}
}